============
Contributing
============
..
    [contributing]

.. highlight:: shell

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

Types of Contributions
----------------------

Report Bugs
~~~~~~~~~~~

Report bugs at <https://gitlab.com/dboe/mpl_pyqtgraph/-/issues/>_.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

If you want quick feedback, it is helpful to mention specific developers
(@devloper_name) or @all. This will trigger a mail to the corresponding developer(s).

Fix Bugs
~~~~~~~~

Look through the repository issues for bugs. Anything tagged with "bug" and "help
wanted" is open to whoever wants to implement it.

Implement Features
~~~~~~~~~~~~~~~~~~

Look through the remote issues for features. Anything tagged with "enhancement"
and "help wanted" is open to whoever wants to implement it.

Write Documentation
~~~~~~~~~~~~~~~~~~~

`mpl_pyqtgraph` could always use more :ref name="Documentation":`documentation`, whether as part of the
official `mpl_pyqtgraph` docs, in docstrings, or even on the web in blog posts,
articles, and such.

Write pytest, unittests or doctests
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`mpl_pyqtgraph` profits a lot from better :ref name="Testing":`testing`. We encourage you to add unittests 
with the `pytest` module (`unittest` module is OK, too) in the `tests` directory or doctests (as part of docstrings or in the documentation).

Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an `Issue <https://gitlab.com/dboe/mpl_pyqtgraph/-/issues/>`_.

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome

Get Started!
------------

Ready to contribute? Here's how to set up `mpl_pyqtgraph` for local development.

1. Fork the `mpl_pyqtgraph` repo.
2. Clone your fork locally::

    $ git clone git@gitlab.com:dboe/mpl_pyqtgraph.git

3. Set up your fork for local development::

    $ cd mpl_pyqtgraph/
    $ pip install .[dev]

4. Step 3. already installed `pre-commit <https://pre-commit.com/>`_. Initialize it by running::

    $ pre-commit install

5. Create a branch for local development::

    $ git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

6. When you're done making changes, check that your changes pass flake8 and the
   tests::

    $ make test

7. Commit your changes and push your branch to origin::

    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature

8. Submit a pull request through the repository website.

Pull Request Guidelines
-----------------------

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should include tests.
2. If the pull request adds functionality, the docs should be updated. Put
   your new functionality into a function with a docstring, and add the
   feature to the list in README.rst.
3. The pull request should work for Python 3.5, 3.6, 3.7 and 3.8, and for PyPy. Check
   <https://gitlab.com/dboe/mpl_pyqtgraph/-/merge_requests/>_
   and make sure that the tests pass for all supported Python versions.

Testing
-------

To run tests, use::

    $ make test

To run a subset of tests, you have the following options::

    $ pytest tests/test_package.py

    $ pytest tests/test_package.py::Test_mpl_pyqtgraph::test_version_type

    $ pytest --doctest-modules docs/usage.rst

    $ pytest --doctest-modules mpl_pyqtgraph/core.py -k "MyClass.funciton_with_doctest"

Use the '--trace' option to directly jump into a pdb debugger on fails. Check out the coverage of your api with::

    $ make coverage

Documentation
-------------
To compile the documentation (including automatically generated module api docs), run::

    $ make docs
    
To run an auto-updating server, use

    $ make docs-serve

Use doctests as much as possible in order to have tested examples in your documentation.

Style guide
-----------
Please follow the `google style guide <https://google.github.io/styleguide/pyguide.html>`_ illustrated
by `this example <https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html>`_.

Deploying
---------

A reminder for the maintainers on how to deploy.
Make sure all your changes are committed.
Then run::

    $ make publish

.. dropdown:: Failed CI deploy stage
    :icon: alert
    
    In case of failure in the CI deploy stage after :code:`make publish`, use the following rule to delete the tag (also remote)

    $ make untag
    
If something fails on the ci side, you can make use of 

The CI will then deploy to PyPI if tests pass.