#!/usr/bin/env python

"""Tests for `mpl_pyqtgraph` package."""

import unittest
import mpl_pyqtgraph


class TestPackage(unittest.TestCase):
    """Tests for `mpl_pyqtgraph` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_version_type(self):
        """Assure that version type is str."""

        self.assertIsInstance(mpl_pyqtgraph.__version__, str)
