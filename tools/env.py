import shutil
from .process import run_shell_command
import tempfile
import os
from ensureconda.resolve import platform_subdir


TMPDIR = tempfile.gettempdir()
BOOTSTRAPDIR = os.path.join(TMPDIR, "bootstrap")
PROJECT_DIR = os.path.abspath(os.path.join(__file__, "../.."))
ENVIRONMENT_YAML = "environment.yml"
ENVIRONMENT_YAML_PATH = os.path.join(PROJECT_DIR, ENVIRONMENT_YAML)
POETRY_LOCK = "poetry.lock"
CONDA_LOCK = f"conda-{platform_subdir()}.lock"


def install(conda_run=""):
    run_shell_command(f"{conda_run}poetry install")
    run_shell_command(f"{conda_run}pre-commit install")


def update(conda_run=""):
    # Re-generate Conda lock file(s) based on environment.yml
    run_shell_command(f"{conda_run}conda-lock -k explicit --conda mamba")
    # Update Conda packages based on re-generated lock file
    run_shell_command(f"mamba update --file {CONDA_LOCK}")
    # Update Poetry packages and re-generate poetry.lock
    run_shell_command(f"{conda_run}poetry update")


def create(name="env"):
    """
    This create routine was initially influenced by
    https://stackoverflow.com/questions/70851048/does-it-make-sense-to-use-conda-poetry
    """
    env_dir = os.path.join(PROJECT_DIR, name)

    if os.path.isfile(os.path.join(env_dir, POETRY_LOCK)):
        run_shell_command(f"conda create -p {env_dir} --file {CONDA_LOCK}")
        install()

    requirements_here = ["poetry", "mamba", "conda-lock"]
    requirements_not_satisfied = any(
        [shutil.which(name) is None for name in requirements_here]
    )

    run = ""
    if requirements_not_satisfied:
        # Use (and if necessary create) a bootstrap env
        if not os.path.exists(BOOTSTRAPDIR):
            run_shell_command(
                f"conda create --yes -p {BOOTSTRAPDIR} -c conda-forge "
                + " ".join(requirements_here)
            )
        run = f"conda run -p {BOOTSTRAPDIR} "

    # Create conda lock file(s) from environment.yml
    run_shell_command(f"{run}conda-lock -k explicit --conda mamba")
    # Add conda-lock (and other packages, as needed) to pyproject.toml and poetry.lock

    run_shell_command(f"{run}mamba env create -f {ENVIRONMENT_YAML} -p {env_dir}")
    run = f"conda run -p {env_dir} "

    install(conda_run=run)

    # Add conda and poetry lock files
    run_shell_command("git add *.lock")

    run_shell_command("git commit -m 'build(conda): lock-files'")
