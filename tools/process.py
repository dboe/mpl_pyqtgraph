import subprocess


def run_shell_command(command_line, *args, **kwargs):
    command_line_args = command_line.split(" ")
    print(f"Run command '{command_line}'")
    subprocess.call(command_line_args, *args, **kwargs)
