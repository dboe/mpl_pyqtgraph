include tools/Makefile.envs

# OPTIONS
part ?= patch

test: FORCE
	flake8 mpl_pyqtgraph tests
	pylint mpl_pyqtgraph tests
	py.test

coverage: env
	# coverage run $(MODULE) test
	py.test --cov=$(MODULE) || true
	# coverage report
	coverage html
	python -m webbrowser htmlcov/index.html

clean:
	coverage erase
	rm -rf htmlcov
	rm -rf docs/_build
	rm -rf dist
	rm -rf build
	rm -rf report
	rm -rf .tox
	rm -rf .pytest_cache
	rm -rf *.egg-info
	# pre-commit clean

publish:
	# call optional with argument: make part=minor publish
    # possible values : major / minor / patch
	# Makefile laggs afterwards with "poetry version --short" properly so we process the output
	$(eval NEW_VERSION := $(shell poetry version $(part) --dry-run -s --no-plugins))
	@echo $(GITSTATUS)
	@if [ -z $(GITSTATUS) ]; then \
	  	echo "Working directory clean."; \
	else \
		git status; \
		printf $(_WARN) "WAIT" "Your Git status is not clean!" ; \
	    if ! $(MAKE) -s confirm ; then \
		    printf $(_ERROR) "KO" "EXIT" ; \
	        exit 1; \
		fi \
	fi
	poetry version $(part)
	git commit -am "release: v$(NEW_VERSION)"
	git tag "v$(NEW_VERSION)"
	git push
	git push --tags


	

untag:
	# remove last tag. mostly, because publishing failed
	printf $(_WARN) "WAIT" "Do you want to delete the current version tag and push the change?" ; \
	if $(make) -s confirm ; then \
		git tag -d v$(VERSION)
		git push origin :refs/tags/v$(VERSION)
	else \
		printf $(_ERROR) "ko" "exit" ; \
		exit 1; \
	fi

requirements: pyproject.toml
	poetry export --without-hashes --format=requirements.txt > requirements.txt
	
docs-clean:
	rm -rf docs/_build

docs-serve:
	${CONDA_RUN}poetry install --only docs
	sphinx-autobuild docs docs/_build/ -j auto --watch my_project
	
docs-build: docs-clean
	${CONDA_RUN}poetry install --only docs
	sphinx-build -M html docs docs/_build/ -E -a -j auto -W --keep-going

docs: docs-build
	# open the html slides
	${CONDA_RUN}python -m webbrowser docs/_build/html/index.html

update:
	# get up to date with the cookiecutter template 'dough'
	# first check that no changes are existing
	@echo $(GITSTATUS)
	@if [ -z $(GITSTATUS) ]; then \
	  	echo "Working directory clean."; \
	else \
		git status; \
	  	echo "Your status is not clean! I can not update!"; \
	    exit 1; \
	fi
	# Starting upgrade
	cookiecutter_project_upgrader

build:
	poetry build

FORCE: ;
