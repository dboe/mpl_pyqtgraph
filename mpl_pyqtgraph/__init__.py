"""Top-level package of mpl_pyqtgraph."""

__author__ = """Daniel Böckenhoff"""
__email__ = "daniel.f.boeckenhoff@gmail.com"
__version__ = "0.0.0"
