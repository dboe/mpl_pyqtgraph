Library documentation
=====================

.. include:: overview/index.rst
.. include:: installation/index.rst
.. include:: usage/index.rst
.. include:: reference/index.rst

.. toctree::
    :caption: Documentation
    :hidden:
    :maxdepth: 2

    overview/index
    installation/index
    usage/index
    reference/index