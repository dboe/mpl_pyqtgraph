========
Overview
========
..
   What is it? Why should I use it?
   [overview-start]

.. include:: ../../../README.rst
   :start-after: [overview-start]
   :end-before: [overview-end]

..
   [overview-end]
