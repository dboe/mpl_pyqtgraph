=====
Usage
=====
..
    How to use? Give me a primer.
    [usage]
    
..
    [essence-start]

To use mpl_pyqtgraph in a project use

.. code-block:: python
   
   import mpl_pyqtgraph
    
..
    [essence-end]
