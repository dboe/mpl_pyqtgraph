=====================
library documentation
=====================

..
    [shields-start]

.. pypi
.. image:: https://img.shields.io/pypi/v/mpl_pyqtgraph.svg
    :target: https://pypi.python.org/pypi/mpl_pyqtgraph
.. ci
.. image:: https://gitlab.com/dboe/mpl_pyqtgraph/badges/master/pipeline.svg
    :target: https://gitlab.com/dboe/mpl_pyqtgraph/-/pipelines/latest

.. latest release
.. image:: https://gitlab.com/dboe/mpl_pyqtgraph/badges/master/release.svg
    :target: https://gitlab.com/dboe/mpl_pyqtgraph/-/releases

.. coverage
.. image:: https://gitlab.com/dboe/mpl_pyqtgraph/badges/master/coverage.svg
    :target: https://gitlab.com/dboe/mpl_pyqtgraph/commits/master

.. docs
.. image:: https://img.shields.io/website-up-down-green-red/https/dboe.gitlab.io/mpl_pyqtgraph.svg?label=docs
    :target: https://dboe.gitlab.io/mpl_pyqtgraph

.. pre-commit
.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :target: https://github.com/pre-commit/pre-commit
   :alt: pre-commit

..
    [shields-end]

Overview
========

..
    [overview-start]

This package provides a pyqtgraph backend for matplotlib with the aim to unify the interface.

..
    [overview-end]

Licensed under the ``MIT License``

Resources
---------

..
    [resources-start]
* Source code: https://gitlab.com/dboe/mpl_pyqtgraph
* Documentation: https://dboe.gitlab.io/mpl_pyqtgraph
* Pypi: https://pypi.python.org/pypi/mpl_pyqtgraph
  
Inspiration is taken from the follopwing proejcts:
* rna
* teili
* https://github.com/epfl-lts2

..
    [resources-end]


Features
--------

..
    [features-start]

The following features should be highlighted:

* TODO

..
    [features-end]
